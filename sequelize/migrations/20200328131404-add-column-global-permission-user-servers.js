/**
 * Adds the  global permission column to the users table. This column
 * will be the permission flag used to determine how the user can interact
 * with the bot on a global level (example... can they access dev commands?)
 *
 * up: Adds the globalPermission column
 * down: Removes the globalPermission column
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('users', 'globalPermission', {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0x00000001,
        })
    },
    down: queryInterface => {
        return queryInterface.removeColumn('users', 'globalPermission')
    },
}
