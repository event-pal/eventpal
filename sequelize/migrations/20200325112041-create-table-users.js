/**
 * Migration for the users table
 *
 * up: Creates a users table
 * down: Drops the users table
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.STRING(18),
                allowNull: false,
                primaryKey: true,
                unique: true,
            },
            timezone: {
                type: Sequelize.STRING(30),
                allowNull: true,
                defaultValue: null,
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true,
                defaultValue: null,
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        })
    },
    down: queryInterface => {
        return queryInterface.dropTable('users')
    },
}
