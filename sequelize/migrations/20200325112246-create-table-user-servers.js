/**
 * Migration for the users table
 *
 * up: Creates a users table
 * down: Drops the users table
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('userServers', {
            userId: {
                type: Sequelize.DataTypes.STRING(18),
                references: {
                    model: 'users',
                    key: 'id',
                },
                allowNull: false,
            },
            serverId: {
                type: Sequelize.DataTypes.STRING(18),
                references: {
                    model: 'servers',
                    key: 'id',
                },
                allowNull: false,
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        })
    },
    down: queryInterface => {
        return queryInterface.dropTable('userServers')
    },
}
