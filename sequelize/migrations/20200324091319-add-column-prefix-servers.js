/**
 * Adds the prefix column to the servers table.
 *
 * up: Adds the column
 * down: Removes the column
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('servers', 'prefix', {
            type: Sequelize.STRING(5),
            allowNull: true,
            default: null,
        })
    },
    down: queryInterface => {
        return queryInterface.removeColumn('servers', 'prefix')
    },
}
