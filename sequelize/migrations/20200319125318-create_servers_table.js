/**
 * Migration for the servers table.
 *
 * up: Creates a server table.
 * down: Drops the server table.
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('servers', {
            id: {
                type: Sequelize.STRING(18),
                allowNull: false,
                primaryKey: true,
                unique: true,
            },
            members: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true,
                default: null,
            },
            preferredTimezone: {
                type: Sequelize.STRING(30),
                allowNull: false,
                defaultValue: 'UTC',
            },
            eventsLimit: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 5,
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        })
    },
    down: queryInterface => {
        return queryInterface.dropTable('servers')
    },
}
