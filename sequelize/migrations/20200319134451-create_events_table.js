/**
 * Migration for the events table.
 *
 * up: Creates an events table.
 * down: Drops the events table.
 */
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('events', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true,
                unique: true,
            },
            serverId: {
                type: Sequelize.STRING(18),
                references: {
                    model: 'servers',
                    key: 'id',
                },
                allowNull: false,
            },
            author: {
                type: Sequelize.STRING(18),
                allowNull: false,
            },
            modifiedBy: {
                type: Sequelize.STRING(18),
                allowNull: false,
            },
            type: {
                type: Sequelize.ENUM(
                    'ONCE',
                    'HOURLY',
                    'DAILY',
                    'WEEKLY',
                    'MONTHLY',
                    'YEARLY'
                ),
                defaultValue: 'ONCE',
                allowNull: false,
            },
            channelId: {
                type: Sequelize.STRING(18),
                allowNull: false,
            },
            msg: {
                type: Sequelize.STRING(1000),
                allowNull: false,
            },
            eventName: {
                type: Sequelize.STRING(50),
                allowNull: false,
            },
            eventDate: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            nextEventTrigger: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            deletedAt: {
                type: Sequelize.DATE,
                allowNull: true,
                default: null,
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        })
    },
    down: queryInterface => {
        return queryInterface.dropTable('events')
    },
}
