/**
 * Represents an object that handles caching and retrieving data
 * from our cache.
 */
class CacheObject {
    /**
     * Caches the object in the cache.
     *
     * @param {Object} obj
     */
    async set(_) {
        return null
    }

    /**
     * Gets the object from the cache if it exists,
     * otherwise returns null.
     *
     * @param {Object} val The value we use to search
     * @param {Function} fdb If we couldn't find this object,
     *   fall-back to this function which retrieves it from the database.
     */
    async get(_) {
        return null
    }

    /**
     * Gets the item from the database, this is a fallback function
     * for when we couldn't find the object in the cache.
     */
    async db(_) {
        return null
    }
}

module.exports = CacheObject
