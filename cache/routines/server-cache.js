const CacheObject = require('../cache-object')
const ServerModel = require('../../models/Servers')
const logger = require('../../utils/logger')
const cacheClient = require('../cache-client')
const { Op } = require('sequelize')
const { initializeServer } = require('../../database/initializers/initServer')

// How long we keeep the server cached for in the database?
const CACHE_SERVER_EXP = 3600
// The tag that we use find the server in the cache.
const CACHE_SERVER_TAG = 'server'

/**
 * Manges the server cache object
 */
class ServerCache extends CacheObject {
    /**
     * Caches the Server in the cache.
     *
     * @param {Object} obj
     */
    async set(server) {
        return new Promise((resolve, reject) => {
            if (!server.id) {
                reject(
                    'Server object must have an id in order to store it in the cache.'
                )
            }
            resolve(
                cacheClient.setex(
                    `${CACHE_SERVER_TAG}:${server.id}`,
                    CACHE_SERVER_EXP,
                    JSON.stringify(server)
                )
            )
        })
    }

    /**
     * Only looks at the cache and tries to find the user.
     *
     * @param {String} serverId
     */
    async getCache(serverId) {
        return new Promise((resolve, reject) => {
            cacheClient.get(
                `${CACHE_SERVER_TAG}:${serverId}`,
                (err, result) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(JSON.parse(result))
                    }
                }
            )
        })
    }

    /**
     * Gets the object from the cache if it exists,
     * otherwise returns null.
     *
     * @param {Object} id The value we use to search
     *   fall-back to this function which retrieves it from the database.
     */
    async get(discordGuild, shouldCreate) {
        try {
            const serverCache = await this.getCache(discordGuild.id)

            if (!serverCache) {
                // If we want to create the server upon there not being one,
                // let's do that now. Else just get the result from the db.
                const serverDB = shouldCreate
                    ? await initializeServer(discordGuild)
                    : await this.getdb(discordGuild.id)

                logger.debug(JSON.stringify(serverDB))

                if (serverDB) {
                    // Found user in the database, let's cache them.
                    try {
                        this.set(serverDB)
                    } catch (err) {
                        logger.error(
                            `Setting server#${discordGuild.id} in cache failed after cache miss: ${err}`
                        )
                    }
                }

                return serverDB
            } else {
                // Cache hit.
                return serverCache
            }
        } catch (err) {
            logger.error(
                `Failed getting server#${discordGuild.id} from the cache: ${err}`
            )
            return null
        }
    }

    /**
     * Gets the item from the database, this is a fallback function
     * for when we couldn't find the object in the cache.
     */
    async getdb(id) {
        try {
            return await ServerModel.Server.findOne({
                where: {
                    id: id,
                    deletedAt: {
                        [Op.eq]: null,
                    },
                },
                raw: true,
            })
        } catch (err) {
            logger.error(
                `Failed getting server#${id} from database in server cache manager: ${err}`
            )
            return null
        }
    }
}

module.exports = ServerCache
