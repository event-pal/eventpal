const CacheObject = require('../cache-object')
const cacheClient = require('../cache-client')
const logger = require('../../utils/logger')
const { initializeUser } = require('../../database/initializers/initUser')
const { User } = require('../../models/User')
const { Op } = require('sequelize')

// How long we keep the user cached for in the database?
const CACHE_USER_EXP = 3600
// The tag that we use find the server in the cache.
const CACHE_USER_TAG = 'user'

/**
 * Manages the cache for the user
 */
class CacheUser extends CacheObject {
    /**
     * Caches the User
     *
     * @param {Object} user
     */
    async set(user) {
        return new Promise((resolve, reject) => {
            if (!user.id) {
                throw new Error(
                    'The user object must have an id in order to cache it.'
                )
            }
            // Set the user in the cache.
            cacheClient.setex(
                `${CACHE_USER_TAG}:${user.id}`,
                CACHE_USER_EXP,
                JSON.stringify(user),
                (err, result) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(result)
                    }
                }
            )
        })
    }

    /**
     * Only attempts to retrieve the user from the cache.
     *
     * @param {String} userId The id of the user
     */
    async getCache(userId) {
        return new Promise((resolve, reject) => {
            cacheClient.get(`${CACHE_USER_TAG}:${userId}`, (err, result) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(JSON.parse(result))
                }
            })
        })
    }

    /**
     * Gets the object from the cache if it exists,
     * otherwise we do a database lookup. Returns the result of the
     * database lookup (either null or object)
     *
     * @param {Object} discordUser The discord user that we're trying
     *   to cache.
     * @param {Boolean} shouldInitialize Should we initialize the user
     *   if they don't already exist?
     */
    async get(discordUser, discordGuild, shouldCreate) {
        try {
            const userCache = await this.getCache(discordUser.id)

            if (!userCache) {
                // Cache miss, try from database
                const userDB = shouldCreate
                    ? await initializeUser(discordUser, discordGuild)
                    : await this.getdb(discordUser.id)

                logger.debug(JSON.stringify(userDB))

                if (userDB) {
                    // If we found a user in the db, let's update our cache.
                    try {
                        this.set(userDB)
                    } catch (err) {
                        // Just log the fact that we had an error, but still return
                        // the successfully obtained value from the db.
                        logger.error(
                            `Setting user#${discordUser.id} in cache failed after cache miss: ${err}`
                        )
                    }
                }

                return userDB
            } else {
                // Cache hit
                return userCache
            }
        } catch (err) {
            logger.error(
                `Failed getting user#${discordUser.id} from the cache: ${err}`
            )
            return null
        }
    }

    /**
     * Gets the item from the database, this is a fallback function
     * for when we couldn't find the object in the cache.
     *
     * @param {String} userId
     */
    async getdb(userId) {
        try {
            return await User.findOne({
                where: {
                    id: userId,
                    deletedAt: {
                        [Op.eq]: null,
                    },
                },
                raw: true,
            })
        } catch (err) {
            logger.error(
                `Failed fetching user#${userId} from database after cache miss: ${err}`
            )
            return null
        }
    }
}

module.exports = CacheUser
