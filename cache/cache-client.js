const logger = require('../utils/logger')
const redis = require('redis')

// Building our redis-cache client.
const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
})

client.on('connect', () => {
    logger.info('Succesfully connected to cache server.')
})

client.on('error', err => {
    logger.error(`Error connecting to cache server: ${err}`)
})

module.exports = client
