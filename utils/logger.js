const log4js = require('log4js')

// Configuration for the logger
log4js.configure({
    appenders: {
        out: { type: 'stdout' },
        app: {
            type: 'dateFile',
            filename: `./logs/eventpal.log`,
            daysToKeep: 10,
            keepFileExt: true,
            pattern: 'yyyy-MM-dd',
        },
    },
    categories: {
        default: {
            appenders: ['out', 'app'],
            level: 'debug',
        },
    },
})

const logger = log4js.getLogger()
// Setting the logger level to debug by default.
// TODO: Perhaps configure this based on .env variable?
logger.level = 'debug'

module.exports = logger
