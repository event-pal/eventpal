const { EVENT_BATCH_SIZE } = require('../discord/client/config')
const { Event } = require('../models/Event')
const { Op } = require('sequelize')
const Queue = require('bull')
const moment = require('moment')
const client = require('../discord/client/eventbot')
const eventEmbed = require('../discord/embeds/event')

/**
 * Finds all the tags in the message
 */
const findTagsRegex = /(<@(&|!)[0-9]{18}>)|(@here)|(@everyone)/g

// The default options we use to render the user / guild avatar.
const defaultIconOptions = {
    format: 'jpg',
    dynamic: true,
    size: 512,
}

/**
 * The queue responsible for emitting events to all our
 * discord servers.
 */
let eventEmitQueue = new Queue('event-emit-queue', {
    redis: {
        port: process.env.REDIS_PORT,
        host: process.env.REDIS_HOST,
        password: process.env.REDIS_PASSWORD,
    },
})

/**
 * The asynchronous task we run.
 */
eventEmitQueue.process(async (job, done) => {
    // Get the time we started our queue.
    const initDate = moment()
    client.logger.info(`Started event emit queue ${initDate.clone().format()}`)

    try {
        const events = await Event.findAndCountAll({
            where: {
                // Make sure this event isn't deleted.
                deletedAt: {
                    [Op.eq]: null,
                },
                // Make sure that this event still has a trigger, and its trigger
                // is less than the current date.
                nextEventTrigger: {
                    [Op.ne]: null,
                    [Op.lte]: moment(),
                },
            },
            // Set a limit on how many events we can process at any given
            // job interval.
            limit: EVENT_BATCH_SIZE,
        })

        client.logger.debug(
            `Pulled ${events.count} events with a max batch of ${EVENT_BATCH_SIZE}`
        )

        /**
         * Looping through all our events, and doing two things.
         *   1) Sending the event reminder to the discord server
         *   2) Updating our database saying we succesfully sent the message.
         */
        events.rows.forEach(async (event, i) => {
            try {
                // Get a reference to the channel we're sending the message to.
                const channel = await client.channels.fetch(event.channelId)
                // The set of all tagged roles / users / globals
                let taggedEntities = new Set()

                // Adding all matches to the set.
                Array.from(event.msg.matchAll(findTagsRegex), m => {
                    taggedEntities.add(m[0])
                })

                // Send a reminder to the channel.
                await channel.send(
                    // Any tags we should send.
                    Array.from(taggedEntities).join(' '),
                    // The embeded event reminder.
                    eventEmbed.craftEmbed(
                        channel && channel.guild
                            ? channel.guild.iconURL(defaultIconOptions)
                            : null,
                        client.user.avatarURL(defaultIconOptions),
                        event
                    )
                )

                // Get the next event trigger.
                const nextTrigger = getNextEventTrigger(
                    event.type,
                    moment(event.nextEventTrigger)
                )

                client.logger.debug(
                    `Updated nextEventTrigger for event#${
                        event.id
                    } to ${nextTrigger.format()}`
                )

                // Update our database with the next event trigger value.
                //   Of course if the event is a one-time event, we set that value to NULL.
                await event.update({
                    nextEventTrigger:
                        event.type === 'ONCE' ? null : nextTrigger,
                })
                // Update our job progress in-case we have someone listening.
                job.progress((i + 1) / events.count)
            } catch (err) {
                client.logger.error(
                    `Failed event emit process for event ${event.id}: ${err.stack}`
                )
            }
        })

        // Done with the task.
        done(null, {
            events: events.count,
        })
    } catch (err) {
        client.logger.error(`Event queue failure:\n${err.stack}`)
        done(err, null)
    }
})

eventEmitQueue.on('stalled', (job, err) => {
    // A job has been marked as stalled. This is useful for debugging job
    // workers that crash or pause the event loop.
    client.logger.error(`Event emit queue with id ${job.id} stalled: ${err}`)
})

eventEmitQueue.on('progress', (job, progress) => {
    // A job's progress was updated!
    client.logger.info(
        `Event emit queue with id ${job.id}, updated progress: ${progress *
            100}%`
    )
})

eventEmitQueue.on('completed', (job, result) => {
    // A job successfully completed with a `result`.
    client.logger.info(
        `Event emit queue with id ${job.id}, finished emitting ${result.events} events.`
    )
})

eventEmitQueue.on('failed', (job, err) => {
    client.logger.info(
        `Event emit queue with id ${job.id}, failed emit batch: ${err}`
    )
})

/**
 * Builds the next timestamp for when the event will fire.
 */
const getNextEventTrigger = (eventType, date) => {
    switch (eventType) {
        case 'HOURLY':
            return date.clone().add(1, 'hour')
        case 'DAILY':
            return date.clone().add(1, 'day')
        case 'WEEKLY':
            return date.clone().add(1, 'week')
        case 'MONTHLY':
            return date.clone().add(1, 'month')
        case 'YEARLY':
            return date.clone().add(1, 'year')
        default:
            return date.clone()
    }
}

module.exports = eventEmitQueue
