const client = require('../discord/client/eventbot')
const Queue = require('bull')
const { Op } = require('sequelize')
const { Event } = require('../models/Event')
const { RELEASE_TAG, PROJECT_LINK } = require('../discord/client/config')

/**
 * All the different ways we can update our status.
 */
const STATUS_JOB_TYPES = {
    updateTag: 'update:tag',
    updateServers: 'update:servers',
    updateEvents: 'update:events',
}

/**
 * The order for our status changes.
 */
let JOB_ORDER = [
    STATUS_JOB_TYPES.updateTag,
    STATUS_JOB_TYPES.updateServers,
    STATUS_JOB_TYPES.updateEvents,
]

// How often should we run the status change.
const STATUS_SWITCH_OFFSET = 45000

/**
 * The queue responsible for emitting events to all our
 * discord servers.
 */
let botStatusQueue = new Queue('bot-status-queue', {
    redis: {
        port: process.env.REDIS_PORT,
        host: process.env.REDIS_HOST,
        password: process.env.REDIS_PASSWORD,
    },
})

botStatusQueue.process(async (job, done) => {
    // The status name we're setting for the bot.
    let statusName = ''
    // Takes out the first item in the array
    const currentJob = JOB_ORDER.shift()
    // Then we push it back to the end
    JOB_ORDER.push(currentJob)

    client.logger.info(
        `Updating bot status CRON job: ${job.id} type=${currentJob}`
    )

    try {
        if (currentJob === STATUS_JOB_TYPES.updateTag) {
            statusName = RELEASE_TAG
        } else if (currentJob === STATUS_JOB_TYPES.updateServers) {
            statusName = `${client.guilds.cache.size} Servers`
        } else if (currentJob === STATUS_JOB_TYPES.updateEvents) {
            const eventCount = await Event.count({
                where: {
                    deletedAt: {
                        [Op.eq]: null,
                    },
                },
            })
            statusName = `${eventCount} Events`
        }
    } catch (err) {
        client.logger.error(
            `Couldn't update bot's status during ${job.data.type} job: ${err}`
        )
        return done(err, null)
    }

    try {
        // Updating the status
        await client.user.setPresence({
            activity: {
                name: statusName,
                type: 'PLAYING',
                url: PROJECT_LINK,
            },
            status: 'online',
        })
        done(null, true)
    } catch (err) {
        client.logger.error(`Failed setting bot status on discord: ${err}`)
        done(err, null)
    }
})

module.exports = { botStatusQueue, STATUS_JOB_TYPES, STATUS_SWITCH_OFFSET }
