/**
 * This file keeps some constants which are global, and should be
 * accessable to all other models.
 */
module.exports = {
    SNOWFLAKE_LENGTH: 18,
    TZ_MAX_LENGTH: 30,
}
