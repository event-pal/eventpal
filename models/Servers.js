const { SNOWFLAKE_LENGTH } = require('./GlobalModel')
const Sequelize = require('sequelize')
const sequelize = require('../database/connection')

/**
 * The constant properties used by this model.
 */
module.exports.props = {
    PREFERRED_TIMEZONE_LENGTH: 30,
    DEFAULT_TIMEZONE: 'UTC',
    DEFAULT_EVENTS_LIMIT: 5,
    MAX_PREFIX_LENGTH: 5,
    MIN_PREFIX_LENGTH: 1,
}

/**
 * Servers sequelize model
 *
 * Contains the preferences and statistics about the
 * discord server that the bot is registerred on.
 */
module.exports.Server = sequelize.define('servers', {
    id: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        allowNull: false,
        primaryKey: true,
        unique: true,
    },
    members: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    preferredTimezone: {
        type: Sequelize.STRING(this.props.PREFERRED_TIMEZONE_LENGTH),
        allowNull: false,
        defaultValue: this.props.DEFAULT_TIMEZONE,
    },
    prefix: {
        type: Sequelize.STRING(this.props.MAX_PREFIX_LENGTH),
        allowNull: true,
        defaultValue: null,
    },
    eventsLimit: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: this.props.DEFAULT_EVENTS_LIMIT,
    },
    deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        default: null,
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
})

module.exports.validators = {
    // Validates input for a custom prefix
    prefix: s => {
        // All of the errors we found.
        let err = []

        if (typeof s !== 'string') {
            err.push({
                title: 'Invalid Prefix',
                msg: `The prefix should be a string.`,
            })
        } else if (
            s.length < this.props.MIN_PREFIX_LENGTH ||
            s.length > this.props.MAX_PREFIX_LENGTH
        ) {
            err.push({
                title: 'Invalid Prefix',
                msg: `The prefix has to be between ${this.props.MIN_PREFIX_LENGTH} and ${this.props.MAX_PREFIX_LENGTH} characters.`,
            })
        }

        return err
    },
}
