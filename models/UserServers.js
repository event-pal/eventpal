const Sequelize = require('sequelize')
const sequelize = require('../database/connection')
const { SNOWFLAKE_LENGTH } = require('./GlobalModel')
const { Server } = require('./Servers')
const { User } = require('./User')

/**
 * UserServers model
 *
 * Primarily used to map the many-to-many relationship between
 * users and servers. A server can belong to many users,
 * but a user can also belong to many servers.
 */
module.exports.UserServers = sequelize.define('userServers', {
    userId: {
        type: Sequelize.DataTypes.STRING(SNOWFLAKE_LENGTH),
        references: {
            model: User,
            key: 'id',
        },
        allowNull: false,
    },
    serverId: {
        type: Sequelize.DataTypes.STRING(SNOWFLAKE_LENGTH),
        references: {
            model: Server,
            key: 'id',
        },
        allowNull: false,
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
})
