const Sequelize = require('sequelize')
const sequelize = require('../database/connection')
const { Server } = require('./Servers')
const { validators } = require('./Event')
const { SNOWFLAKE_LENGTH, TZ_MAX_LENGTH } = require('./GlobalModel')

/**
 * The constant properties used by the User model
 */
module.exports.props = {
    PREFERRED_TIMEZONE_LENGTH: 30,
}

/**
 * Sequelize User model
 */
module.exports.User = sequelize.define('users', {
    id: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        allowNull: false,
        primaryKey: true,
        unique: true,
    },
    timezone: {
        type: Sequelize.STRING(TZ_MAX_LENGTH),
        allowNull: true,
        defaultValue: null,
    },
    globalPermission: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0x00000001,
    },
    deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
})

/**
 * Sequelize associations
 */
this.User.belongsToMany(Server, { through: 'userServers' })
Server.belongsToMany(this.User, { through: 'userServers' })

/**
 * Validators for column inputs
 */
module.exports.validators = {
    timezone: validators.eventDate,
}
