const fs = require('fs')
const moment = require('moment')
const ServerModel = require('../Servers')
const EventModel = require('../Event')
const { LOGS_DIR } = require('../../discord/client/config')

// Validates the id used for events
module.exports.validateEventId = id => {
    const tryParse = parseInt(id)

    if (isNaN(tryParse)) {
        return [
            {
                title: 'Invalid Event Id',
                msg: 'The event id should be a number.',
            },
        ]
    } else if (tryParse < 0) {
        return [
            {
                title: 'Invalid Event Id',
                msg: 'The event id cannot be a negative number.',
            },
        ]
    }

    return []
}

module.exports.validateEventName = eventName => {
    // All of the errors we found.
    let err = []

    // Checking type
    if (eventName) {
        // Verifying the length of the string.
        if (eventName.length > EventModel.props.REMINDER_TITLE_LENGTH) {
            err.push({
                title: 'Invalid Event Title',
                msg: `The event name must exist and cannot be longer than ${this.props.REMINDER_TITLE_LENGTH} characters.`,
            })
        }
    } else {
        err.push({
            title: 'Invalid Event Name',
            msg: 'The input argument should be of type string.',
        })
    }

    return err
}

module.exports.validateEventType = s => {
    // Verifies the type is a part of the enum.
    if (!EventModel.props.EVENT_TYPE.includes(s)) {
        return [
            {
                title: 'Invalid Event Type',
                msg: `You must include one of the following event types: ${EventModel.props.EVENT_TYPE.toString()}`,
            },
        ]
    }
    return []
}

module.exports.validateChannelId = channels => {
    // All of the errors we found.
    let err = []

    if (Array.isArray(channels)) {
        if (channels.length !== 1) {
            err.push({
                title: 'Invalid Event Channel',
                msg:
                    'You must mention 1 channel so we know where to send the reminder to.',
            })
        }
    } else {
        err.push({
            title: 'Invalid Channel Type',
            msg: `Channels must be passed in as an array type.`,
        })
    }

    return err
}

module.exports.validateDateFuture = (s, format, tz) => {
    // All of the errors we found.
    let err = []
    // The date parse through momentjs
    const date = moment.tz(s, format, tz)

    if (!date.isValid()) {
        err.push({
            title: `Invalid Event Date`,
            msg: `Your supplied date was invalid or had an invalid format. Here's how the date should look: ${format}`,
        })
    } else if (date.diff(moment().tz(tz)) < 0) {
        err.push({
            title: `Invalid Event Date`,
            msg: `You supplied a start time for the event which is in the past. Please make sure you schedule the event for some time in the future.`,
        })
    }

    return err
}

module.exports.validateEventMsg = s => {
    // All of the errors we found.
    let err = []

    if (!s || s.length === 0) {
        err.push({
            title: 'Invalid Event Message',
            msg: 'You must include a message with your reminder.',
        })
    } else if (s.length > EventModel.props.REMINDER_MSG_LENGTH) {
        err.push({
            title: 'Invalid Event Message',
            msg: `An event message cannot be longer than ${EventModel.props.REMINDER_MSG_LENGTH} characters.`,
        })
    }

    return err
}

module.exports.validateCommandPrefix = s => {
    // All of the errors we found.
    let err = []

    if (typeof s !== 'string') {
        err.push({
            title: 'Invalid Prefix',
            msg: `The prefix should be a string.`,
        })
    } else if (
        s.length < ServerModel.props.MIN_PREFIX_LENGTH ||
        s.length > ServerModel.props.MAX_PREFIX_LENGTH
    ) {
        err.push({
            title: 'Invalid Prefix',
            msg: `The prefix has to be between ${ServerModel.props.MIN_PREFIX_LENGTH} and ${ServerModel.props.MAX_PREFIX_LENGTH} characters.`,
        })
    }

    return err
}

// Validators
module.exports.validateTimezone = s => {
    if (!moment.tz.zone(s)) {
        return [
            {
                title: 'Invalid Timezone',
                msg: 'The timezone provided was invalid.',
            },
        ]
    }
    return []
}

// Delete types for the validate delete type validator
const DELETE_TYPES = ['soft', 'hard']
module.exports.validateDeleteType = s => {
    if (!DELETE_TYPES.includes(s)) {
        return [
            {
                title: 'Invalid Delete Type',
                msg: `The only valid delete types are ${DELETE_TYPES.toString()}`,
            },
        ]
    }
    return []
}

// Timezone types for the timezone validator
//  Either the server preferred timezone, or the user preferred timezone
const TIMEZONE_TYPES = ['server', 'me']
module.exports.validateTimezoneType = t => {
    if (!TIMEZONE_TYPES.includes(t)) {
        return [
            {
                title: 'Invalid Timezone Type',
                msg: `The only valid timezone types are ${TIMEZONE_TYPES.toString()}`,
            },
        ]
    }
    return []
}

module.exports.validateLogFilePath = p => {
    try {
        // List of errors we encountered.
        let errors = []
        // Make sure we're dealing with a string
        const pstr = p.toString()
        // Did we find the file with that name?
        let foundFile = false

        fs.readdirSync(LOGS_DIR).forEach(file => {
            // Found the file.
            if (file === pstr) {
                foundFile = true
                // Sanity check, make sure we're only allowing log files to be
                // validated.
                if (!file.endsWith('.log')) {
                    errors.push({
                        title: 'Invalid File',
                        msg:
                            'For security reasons, pulling non .log files is not allowed.',
                    })
                }
            }
        })

        if (!foundFile) {
            errors.push({
                title: 'File Not Found',
                msg: "That log file doesn't exist.",
            })
        }

        return errors
    } catch (err) {
        return [
            {
                title: 'Error Reading Log Directory',
                msg: `${err.message}`,
            },
        ]
    }
}
