const Sequelize = require('sequelize')
const sequelize = require('../database/connection')
const { Server } = require('./Servers')
const { SNOWFLAKE_LENGTH } = require('./GlobalModel')
const moment = require('moment')

/**
 * The constant properties used by this model.
 */
module.exports.props = {
    EVENT_TYPE: ['ONCE', 'HOURLY', 'DAILY', 'WEEKLY', 'MONTHLY', 'YEARLY'],
    REMINDER_MSG_LENGTH: 1000,
    REMINDER_TITLE_LENGTH: 50,
}

// Event model
module.exports.Event = sequelize.define('events', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
    },
    serverId: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        references: {
            model: Server,
            key: 'id',
        },
        allowNull: false,
    },
    author: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        allowNull: false,
    },
    modifiedBy: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        allowNull: false,
    },
    type: {
        type: Sequelize.ENUM(this.props.EVENT_TYPE),
        defaultValue: this.props.EVENT_TYPE[0],
        allowNull: false,
    },
    channelId: {
        type: Sequelize.STRING(SNOWFLAKE_LENGTH),
        allowNull: false,
    },
    msg: {
        type: Sequelize.STRING(this.props.REMINDER_MSG_LENGTH),
        allowNull: false,
    },
    eventName: {
        type: Sequelize.STRING(this.props.REMINDER_TITLE_LENGTH),
        allowNull: false,
    },
    eventDate: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    nextEventTrigger: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        default: null,
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE,
})

module.exports.validators = {
    // Verifies an id.
    id: i => {
        // All of the errors we found.
        let err = []

        if (isNaN(parseInt(i))) {
            err.push({
                title: 'Invalid Event Id',
                msg: `The event id should be a number.`,
            })
        }

        return { valid: err.length === 0, errs: err }
    },
    // Verifies the event name.
    eventName: s => {
        // All of the errors we found.
        let err = []

        // Checking type
        if (s) {
            // Verifying the length of the string.
            if (s.length > this.props.REMINDER_TITLE_LENGTH) {
                err.push({
                    title: 'Invalid Event Title',
                    msg: `The event name must exist and cannot be longer than ${this.props.REMINDER_TITLE_LENGTH} characters.`,
                })
            }
        } else {
            err.push({
                title: 'Invalid Event Name',
                msg: 'The input argument should be of type string.',
            })
        }

        return { valid: err.length === 0, errs: err }
    },
    // Verifies the type
    type: s => {
        // All of the errors we found.
        let err = []

        // Verifies the type is a part of the enum.
        if (!this.props.EVENT_TYPE.includes(s)) {
            err.push({
                title: 'Invalid Event Type',
                msg: `You must include one of the following event types: ${this.props.EVENT_TYPE.toString()}`,
            })
        }

        return { valid: err.length === 0, errs: err }
    },
    // Verifying the channel id, we take an arry of all mentioned
    // channels as the input argument.
    channelId: channels => {
        // All of the errors we found.
        let err = []

        if (Array.isArray(channels)) {
            if (channels.length !== 1) {
                err.push({
                    title: 'Invalid Event Channel',
                    msg:
                        'You must mention 1 channel so we know where to send the reminder to.',
                })
            }
        } else {
            err.push({
                title: 'Invalid Channel Type',
                msg: `Channels must be passed in as an array type.`,
            })
        }

        return { valid: err.length === 0, errs: err }
    },
    // Validator for the event date.
    eventDate: (s, format, tz) => {
        // All of the errors we found.
        let err = []
        // The date parse through momentjs
        const date = moment.tz(s, format, tz)

        if (!date.isValid()) {
            err.push({
                title: `Invalid Event Date`,
                msg: `Your supplied date was invalid or had an invalid format. Here's how the date should look: ${format}`,
            })
        } else if (date.diff(moment().tz(tz)) < 0) {
            err.push({
                title: `Invalid Event Date`,
                msg: `You supplied a start time for the event which is in the past. Please make sure you schedule the event for some time in the future.`,
            })
        }

        return { valid: err.length === 0, errs: err }
    },
    // Validator for the event message
    msg: s => {
        // All of the errors we found.
        let err = []

        if (!s || s.length === 0) {
            console.log(`Verifying: ${s}`)
            err.push({
                title: 'Invalid Event Message',
                msg: 'You must include a message with your reminder.',
            })
        } else if (s.length > this.props.REMINDER_MSG_LENGTH) {
            err.push({
                title: 'Invalid Event Message',
                msg: `An event message cannot be longer than ${this.props.REMINDER_MSG_LENGTH} characters.`,
            })
        }

        return { valid: err.length === 0, errs: err }
    },
}

// Associations
this.Event.belongsTo(Server, { foreignKey: 'serverId' })
Server.hasMany(this.Event, { foreignKey: 'serverId' })
