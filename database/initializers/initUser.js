const { User } = require('../../models/User')
const { Server } = require('../../models/Servers')
const logger = require('../../utils/logger')

/**
 * Initialize the user in the database
 *
 * Assumes a valid server is present in the database. Since it's
 * possible for guild initialization logic to fail, do not run
 * initialzeUser() under the assumption that a server exists!
 */
const initializeUser = async (discordUser, discordGuild) => {
    try {
        // If we haven't seen this user before, create an entry for them.
        const [user] = await User.findOrCreate({
            where: {
                id: discordUser.id,
            },
        })

        // Find the server that's associated with this discord guild.
        const server = await Server.findOne({
            where: {
                id: discordGuild.id,
            },
        })

        // Does this user have this server as one they've joined?
        const inServer = await user.hasServer(server)

        if (!inServer) {
            // Add the user to this server.
            logger.info(
                `User initilization adding user#${user.id} to server#${server.id}`
            )
            await user.addServer(server)
        } else {
            logger.info(
                `User initialization determined unecessary addition of user#${user.id} in server#${server.id}`
            )
        }

        // Just return the raw JSON object.
        return user.toJSON()
    } catch (err) {
        logger.error(`Failed initializing user in database: ${err}`)
        return null
    }
}

module.exports = {
    initializeUser,
}
