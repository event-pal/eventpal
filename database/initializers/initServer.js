const { Server, props } = require('../../models/Servers')
const logger = require('../../utils/logger')

/**
 * Initialize the server in the database
 */
const initializeServer = async guild => {
    try {
        // Try to find the server, if not create one.
        // Remember a server can be soft-deleted!
        let [server, created] = await Server.findOrCreate({
            where: {
                id: guild.id,
            },
            defaults: {
                id: guild.id,
                members: guild.memberCount,
                preferredTimezone: props.DEFAULT_TIMEZONE,
                eventsLimit: props.DEFAULT_EVENTS_LIMIT,
                deletedAt: null,
            },
        })

        if (!created && server.deletedAt !== null) {
            logger.info(
                `Server initialization determine server#${server.id} existed in database after soft-delete. Reverting soft-delete.`
            )
            // The server did exist, but it was soft-deleted.
            server = await server.update({
                deletedAt: null,
            })
        } else if (created) {
            logger.info(`Server initialized prepared server#${server.id}`)
        }

        return server.toJSON()
    } catch (err) {
        logger.error(`Failed initializing server in database: ${err}`)
        return null
    }
}

module.exports = {
    initializeServer,
}
