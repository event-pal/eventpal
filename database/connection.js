const { Sequelize } = require('sequelize')

// Setting our initial config to empty.
let config = {
    dialect: null,
    seederStorage: null,
    username: null,
    password: null,
    database: null,
    host: null,
    port: null,
}

// Loading our configuration based on the environment.
if (process.env.NODE_ENV === 'development') {
    config = {
        dialect: 'postgres',
        seederStorage: 'sequelize',
        username: process.env.DEV_DB_USERNAME,
        password: process.env.DEV_DB_PASSWORD,
        database: process.env.DEV_DB_NAME,
        host: process.env.DEV_DB_HOSTNAME,
        port: process.env.DEV_DB_PORT,
    }
} else if (process.env.NODE_ENV === 'test') {
    config = {
        dialect: 'postgres',
        seederStorage: 'sequelize',
        username: process.env.TEST_DB_USERNAME,
        password: process.env.TEST_DB_PASSWORD,
        database: process.env.TEST_DB_NAME,
        host: process.env.TEST_DB_HOSTNAME,
        port: process.env.TEST_DB_PORT,
    }
} else if (process.env.NODE_ENV === 'production') {
    config = {
        dialect: 'postgres',
        username: process.env.PROD_DB_USERNAME,
        password: process.env.PROD_DB_PASSWORD,
        database: process.env.PROD_DB_NAME,
        host: process.env.PROD_DB_HOSTNAME,
        port: process.env.PROD_DB_PORT,
    }
}

/**
 * Initializing Sequelize connection
 *
 * TODO: Explore pooling option for clustering.
 */
const sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    {
        dialect: config.dialect,
        host: config.host,
        port: config.port,
        logging: false,
        define: {
            underscored: false,
            freezeTableName: false,
            charset: 'utf8',
        },
    }
)

module.exports = sequelize
