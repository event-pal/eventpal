/**
 * Some constants used across the project
 */
module.exports = {
    PROJECT_LINK: 'https://gitlab.com/event-pal/eventpal',
    SUPPORT_LINK: 'https://gitlab.com/event-pal/eventpal',
    SUPPORT_COMMANDS_LINK: 'https://gitlab.com/event-pal/eventpal',
    SUPPORT_DISCORD_SERVER_LINK: 'https://discord.gg/p5GBnxq',
    PROJECT_ICON_URL:
        'https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png',
    DEFAULT_CMD_PREFIX: '!',
    ICON_SETTINGS: 'https://imgur.com/tzMNUwa.jpg',
    ICON_NOTIFICATION: 'https://imgur.com/7qwFlDL.jpg',
    // How often should we emit events (milliseconds).
    EVENT_EMIT_RATE: 100000,
    // How many events can we process in a given batch?
    EVENT_BATCH_SIZE: 100,
    EMBED_COLOR_DARK: '#2A6DF1',
    RELEASE_TAG: `${require('../../package.json').version}-${process.env
        .CI_COMMIT_SHORT_SHA || 'local'}-${process.env.CI_COMMIT_REF_NAME ||
        'local'} (${process.env.NODE_ENV})`,
    // The path to where the logs should be stored.
    LOGS_DIR: './logs/',
    // The global permission system for eventpal.
    GLOBAL_PERMISSIONS: {
        // Allows user to access all default commands
        USER_DEFAULT: 0x00000001,
        // Allows a user to access developer commands
        DEVELOPER: 0x00000002,
    },
}
