const DEFAULT_SEPARATOR = '--'

/**
 * The flag - containing the name and a validator
 */
class Flag {
    /**
     * Builds the flag.
     *
     * @param {String} name
     * @param {Function} validator
     */
    constructor(name, validator) {
        if (typeof name !== 'string') {
            throw Error('The flag name must be a string.')
        }

        this.name = name
        this.validator = validator
    }
}

/**
 * Positional Command Parser
 */
class PositionalParser {
    /**
     * Builds the positional parser.
     *
     * @param {Flag[]} flags An array of strings representing the flags.
     * @param {String} separator The string that prepends each flag: commonly '--'
     *
     */
    constructor(flags, separator) {
        if (!Array.isArray(flags)) {
            throw Error('Flags should be an array of objects.')
        } else if (typeof separator !== 'string') {
            throw Error('The separator must be a string.')
        }

        this.flags = flags
        this.separator = separator
    }

    /**
     * Parses the arguments, maps each positional argument to an array
     * of strings.
     * @param {String[]} args
     */
    parse(args) {
        if (!Array.isArray(args)) {
            throw Error('The parser must recieve an array as an input')
        }
        // Any errors we encounter during parsing.
        let parserErrors = []
        // The parsed object.
        let parsed = {}
        // The current flag we're parsing data for.
        let currFlag = null

        args.forEach(arg => {
            if (arg.startsWith(this.separator)) {
                const flag_str = arg.substring(this.separator.length)
                const flag = this.flags.find(f => f.name === flag_str)
                // Our flag is indeed a valid value.
                if (flag) {
                    // We'll initialize the value for this flag as an empty array.
                    parsed[flag_str] = []
                    // Set the current modififier we're working on to the flag.
                    currFlag = flag
                }
            } else {
                // Which flag are we currently handling, if any?
                if (currFlag) {
                    parsed[currFlag.name].push(arg)
                }
            }
        })

        // Loop through all object keys and verify their
        // arguments through a validator - if it was passed to us.
        Object.keys(parsed).forEach(key => {
            const flag = this.flags.find(f => f.name === key)
            // Join the arguments together.
            parsed[key] = parsed[key].join(' ')

            if (flag.validator) {
                // Parse the string.
                parserErrors = parserErrors.concat(
                    flag.validator(parsed[key]).errs
                )
            }
        })

        return {
            // The structure of the command with conditional flags parsed.
            parsed: parsed,
            // The errors we encountered thanks to the validators.
            errors: parserErrors,
        }
    }
}

/**
 *
 * @param {Object} argumentLayout
 * @param {Object} options
 * @param {String[]} args
 */
const parse = (argumentLayout, options, args) => {
    // The parsed values
    let parsed = {}
    // The errors returned either from the validator or the parser itself
    let errors = []
    // The last valid positonal argument we found.
    let activeKey = null
    // The amount of keys containing some data
    let emptyArgs = 0

    // Setting all of the positional arguments we plan to parse
    // to null.
    Object.keys(argumentLayout).forEach(posArg => {
        parsed[posArg] = null
    })

    args.forEach(arg => {
        if (
            arg.startsWith(
                options.separator ? options.separator : DEFAULT_SEPARATOR
            )
        ) {
            // The flag with the separator removed.
            const fstr = arg.substring(
                options.separator
                    ? options.separator.length
                    : DEFAULT_SEPARATOR.length
            )

            // Check if this flag exists.
            if (argumentLayout[fstr] && parsed[fstr] === null) {
                activeKey = fstr
                parsed[fstr] = []

                return
            }
        }

        if (activeKey) {
            parsed[activeKey].push(arg)
        }
    })

    // Do validation on the data we just parsed.
    Object.keys(parsed).forEach(parsedArg => {
        // Was this argument passed
        if (parsed[parsedArg]) {
            // Was the input for this positional argument empty?
            if (parsed[parsedArg].length === 0) {
                if (argumentLayout[parsedArg].canBeEmpty) {
                    // If this argument has a default value set it here.
                    if (argumentLayout[parsedArg].default) {
                        parsed[parsedArg] = argumentLayout[parsedArg].default
                    }
                } else {
                    // Argument cannot be empty.
                    errors.push({
                        title: `${
                            options.separator
                                ? options.separator
                                : DEFAULT_SEPARATOR
                        }${parsedArg}`,
                        msg: 'This argument cannot be empty.',
                    })
                }
            } else if (argumentLayout[parsedArg].validator) {
                // Combine any errors from this validator with the accumulated errors
                errors = errors.concat(
                    argumentLayout[parsedArg].validator(
                        parsed[parsedArg].join(' ')
                    )
                )
            }
        } else {
            emptyArgs++

            if (argumentLayout[parsedArg].required) {
                // Argument wasn't passed but was required.
                errors.push({
                    title: `${
                        options.separator
                            ? options.separator
                            : DEFAULT_SEPARATOR
                    }${parsedArg}`,
                    msg: 'This argument is marked as required.',
                })
            }
        }
    })

    // If all of our arguments were empty, but we have the requireOne options
    // set to true, we need to report an error.
    if (options.requireOne && emptyArgs === Object.keys(parsed).length) {
        // Argument wasn't passed but was required.
        errors.push({
            title: 'All Arguments Empty',
            msg: 'At least one positional argument must be supplied.',
        })
    }

    return [parsed, errors]
}

/**
 * Makes a parsed result database ready for an insert.
 */
const craftParsedNameSpaces = (argumentLayout, parsed) => {
    const nameSpaces = {}

    // Setup the name-spaces
    Object.keys(argumentLayout).forEach(arg => {
        const nameSpace = argumentLayout[arg].nameSpace
        if (nameSpace && !nameSpaces[nameSpace]) nameSpaces[nameSpace] = {}
    })

    // Pass in the changed values if they exist
    Object.keys(parsed).forEach(parsedArg => {
        const nameSpace = argumentLayout[parsedArg].nameSpace
        // Does this argument have a namespace
        if (nameSpace) {
            // Insert if there's data OR we allow null values
            if (
                parsed[parsedArg] ||
                (parsed[parsedArg] === null &&
                    argumentLayout[parsedArg].canBeEmpty)
            ) {
                const fieldName = argumentLayout[parsedArg].fieldName
                nameSpaces[nameSpace][
                    fieldName ? fieldName : parsedArg
                ] = parsed[parsedArg] ? parsed[parsedArg].join(' ') : null
            }
        }
    })

    return nameSpaces
}

module.exports = { PositionalParser, Flag, parse, craftParsedNameSpaces }
