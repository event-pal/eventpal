const logger = require('../../utils/logger')
const Discord = require('discord.js')

// The token our bot uses to communicate with the Discord API.
const DISCORD_TOKEN = process.env.DISCORD_TOKEN

const bot = new Discord.Client()
// Setting our log4js logger.
bot.logger = logger
// Setting up our commands collection.
bot.commands = new Discord.Collection()

// Login with the bot.
bot.login(DISCORD_TOKEN).catch(err => {
    bot.logger.error(`Error authenticated with Discord: ${err.message}`)
    process.exit(-1)
})

module.exports = bot
