const Discord = require('discord.js')
const {
    RELEASE_TAG,
    ICON_NOTIFICATION,
    EMBED_COLOR_DARK,
} = require('../client/config')

const DEFAULT_EVENT_NAME = 'Untitled Event'
const DEFAULT_EVENT_DESC = 'Empty event description.'

module.exports.craftEmbed = (guildIcon, botIcon, event) => {
    return new Discord.MessageEmbed()
        .setColor(EMBED_COLOR_DARK)
        .setAuthor(
            `Event ›› ${event.eventName || DEFAULT_EVENT_NAME}`,
            guildIcon.toString() || ICON_NOTIFICATION
        )
        .setDescription(event.msg || DEFAULT_EVENT_DESC)
        .setFooter(RELEASE_TAG, botIcon)
        .setTimestamp(Date.now())
}
