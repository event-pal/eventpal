const Discord = require('discord.js')
const {
    PROJECT_LINK,
    RELEASE_TAG,
    EMBED_COLOR_DARK,
} = require('../client/config')

const EMBED_TITLE = 'Settings'
const DEFAULT_SETTING_TITLE = 'Undefined Setting'
const DEFAULT_SETTING_VALUE = 'NULL'
const HELP_TEXT = `Troubles configuring eventpal? Not to worry, we have plenty of examples on our [documentation](${PROJECT_LINK}) page.`

module.exports.craftEmbed = (iconBot, iconUser, username, settings) => {
    let embed = new Discord.MessageEmbed()
        .setColor(EMBED_COLOR_DARK)
        .setAuthor(`${username}  ››  ${EMBED_TITLE}`, iconUser)
        .setDescription(HELP_TEXT)
        .setFooter(RELEASE_TAG, iconBot)
        .setTimestamp(Date.now())
    // Add a field for each setting given to us.
    if (settings && settings.length > 0) {
        settings.forEach(setting => {
            if (setting) {
                embed.addField(
                    setting.setting || DEFAULT_SETTING_TITLE,
                    `››   ${setting.value}\t\t\t` || DEFAULT_SETTING_VALUE,
                    true
                )
            }
        })
    }

    //embed.addField(HELP_TITLE, HELP_TEXT, true)

    return embed
}
