const Discord = require('discord.js')
const {
    RELEASE_TAG,
    SUPPORT_LINK,
    PROJECT_ICON_URL,
    EMBED_COLOR_DARK,
} = require('../client/config')

const TITLE_PREPEND = 'Error:'
const HELP_TEXT_TITLE = "What's Next?"
const HELP_TEXT_CONTENT = `If you believe this is an error on our end, please report it to us on our issue tracker:\n${SUPPORT_LINK}`
const DEFAULT_TITLE = 'Undefined Error Title'
const DEFAULT_HELP_TEXT = 'Unknown error.'

module.exports.craftEmbed = ({
    title,
    description,
    supportMessage,
    date,
    errors,
}) => {
    let embed = new Discord.MessageEmbed()
        .setColor(EMBED_COLOR_DARK)
        .setTitle(`${TITLE_PREPEND} ${title || DEFAULT_TITLE}`)
        .setDescription(description || DEFAULT_HELP_TEXT)
        .setFooter(RELEASE_TAG, PROJECT_ICON_URL)
        .setTimestamp(date || Date.now())
    // Add a field for each error message given to us.
    if (errors && errors.length > 0) {
        errors.forEach(error => {
            if (error) {
                embed.addField(
                    error.title || DEFAULT_TITLE,
                    error.msg || DEFAULT_HELP_TEXT,
                    false
                )
            }
        })
    }
    // Adding the help field. This field tells the users where they can find
    // our help forum.
    embed.addField(HELP_TEXT_TITLE, supportMessage || HELP_TEXT_CONTENT, false)

    return embed
}
