const Discord = require('discord.js')
const client = require('../client/eventbot')
const { RELEASE_TAG, EMBED_COLOR_DARK } = require('../client/config')

const DEFAULT_INFO_TITLE = 'Untitled Info'
const DEFAULT_INFO_DESC = 'Empty info description.'

// The default options we use to render the user / guild avatar.
const defaultIconOptions = {
    format: 'jpg',
    dynamic: true,
    size: 512,
}

module.exports.craftEmbed = ({
    infoTitle,
    infoDescription,
    fields,
    guildIcon,
}) => {
    let embed = new Discord.MessageEmbed()
        .setColor(EMBED_COLOR_DARK)
        .setAuthor(
            infoTitle || DEFAULT_INFO_TITLE,
            guildIcon || client.user.avatarURL(defaultIconOptions)
        )
        .setDescription(infoDescription || DEFAULT_INFO_DESC)
        .setFooter(RELEASE_TAG, client.user.avatarURL(defaultIconOptions))
        .setTimestamp(Date.now())

    // Add a field for each error message given to us.
    if (fields && fields.length > 0) {
        fields.forEach(field => {
            if (field) {
                embed.addField(
                    field.title || DEFAULT_INFO_TITLE,
                    field.value || DEFAULT_INFO_DESC,
                    field.inline || false
                )
            }
        })
    }

    return embed
}
