## EventPal - [CHANGELOG.md](./CHANGELOG.md)

EventPal is an open source discord bot that manges your reminders for you. Set one-time, hourly, weekly, monthly, or even yearly reminders to keep you and your discord server on track.

- Issue or suggestion? Report it here : https://gitlab.com/event-pal/eventpal/-/issues
- Having troubles running the project locally? : https://nafana.gitbook.io/eventpal/

### 1. Development Tools

Dependencies:
- NodeJS 12.16.1 (https://nodejs.org/en/download/)
- Yarn (https://classic.yarnpkg.com/en/docs/install)
- Docker (https://docs.docker.com/install/)
- docker-compose (https://docs.docker.com/compose/install/)
- git (https://git-scm.com/downloads) : for Mac or Linux use package registry

Behind the scenes (You do not need to install these. They're built with our docker-compose):
- Postgres - Database Server
- Redis - Cache Server

We use NodeJS to develop the core functionality for our bot. Yarn is the package manager. Docker, and docker-compose allow us to work in containerized environments
Open a terminal and run the following commands to check if you installed the tools properly

### 2. Before You Start

After cloning the repository create a folder inside the root project directory called `volumes`, and inside that folder create a folder called `postgres`. This folder will contain the files for the database, and docker will use this volume so you don't lose your data everytime you restart the docker image.

```
(root folder)
└── volumes
    └── postgres
```

Now create a file called `.env`. You'll notice there's a file in the root project directory called .env.sample. That gives you a template for what enviornment variables you need to configure.\

For development, it's often enough to have the following:

```bash
# Discord's Authentication Token
# How to get it: https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token
DISCORD_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# The enviornment to run NodeJS in.
NODE_ENV=development

# The directory on which we mount the postgres volume.
POSTGRES_DOCKER_VOL=/that/folder/you/created

# Cache Server credentials
# The host is redis instead of localhost since we plan to run Redis in a docker container.
REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=
REDIS_DB=

# Development credentials for Postgres
# The hostname is postgres instead of localhost since we plan to run Postgres in a docker container.
DEV_DB_USERNAME=postgres
DEV_DB_PASSWORD=postgres
DEV_DB_NAME=postgres
DEV_DB_HOSTNAME=postgres
DEV_DB_PORT=5432
```

### 3. Running the Bot Locally

Run the docker-compose file.

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build --detach
```

To stop all local containers simply run:

```bash
docker-compose down --volumes --remove-orphans
```

### Development Help

For a more in-depth tutorial visit our documentation: https://nafana.gitbook.io/eventpal/