// Set up the enviornment variables from the config.
require('dotenv').config()

const fs = require('fs')
const bot = require('./discord/client/eventbot')

/**
 * This function reads the commands directory and dynamically
 * imports all of the files.
 *
 * NOTE: The command file needs to be named the exact
 *   name you want the command to have.
 *
 * Taken from: https://anidiots.guide/first-bot/a-basic-command-handler
 *   Works well for our bot, no reason to re-invent the wheel.
 */
fs.readdir('./commands/', (err, files) => {
    if (err) return
    files.forEach(file => {
        if (!file.endsWith('.js')) return
        // Load the command file itself
        let props = require(`./commands/${file}`)
        // Get just the command name from the file name
        let commandName = file.split('.')[0]
        bot.logger.info(`Attempting to load command ${commandName}`)
        // Here we simply store the whole thing in the command collection.
        bot.commands.set(commandName, props)
    })
})

/**
 * This function reads the events directory and dynamically
 * imports all of the files.
 *
 * NOTE: The command file needs to be named the exact
 *   name of the event handler provided by DiscordJS. Read more about all the events here:
 *   https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-channelCreate
 *
 * Taken from: https://anidiots.guide/first-bot/a-basic-command-handler
 *   Works well for our bot, no reason to re-invent the wheel.
 */
fs.readdir('./events/', (err, files) => {
    if (err) return bot.logger.error(err)
    files.forEach(file => {
        // If the file is not a JS file, ignore it (thanks, Apple)
        if (!file.endsWith('.js')) return
        // Load the event file itself
        const event = require(`./events/${file}`)
        // Get just the event name from the file name
        let eventName = file.split('.')[0]
        // super-secret recipe to call events with all their proper arguments *after* the `client` var.
        // without going into too many details, this means each event will be called with the client argument,
        // followed by its "normal" arguments, like message, member, etc etc.
        // This line is awesome by the way. Just sayin'.
        bot.on(eventName, event.bind(null, bot))
        // Loading event.
        bot.logger.info(`Attempting to load event ${eventName}`)
        // Deleting require cache.
        delete require.cache[require.resolve(`./events/${file}`)]
    })
})
