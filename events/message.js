const { DEFAULT_CMD_PREFIX } = require('../discord/client/config')
const ServerCache = require('../cache/routines/server-cache')
const UserCache = require('../cache/routines/user-cache')

// Discord's permission value needed to run this command.
const ADMIN_PERMISSION = 0x00000008

// The server cache manager
const serverCacheManager = new ServerCache()
// The user cache manager
const userCacheManager = new UserCache()

module.exports = async (client, message) => {
    // Ignore all bots
    if (message.author.bot) return
    // Non-admin users won't be allowed to execute any commands.
    // TODO: Add a custom permission system, and verify permissions against it.
    if (!message.member || !message.member.hasPermission(ADMIN_PERMISSION))
        return

    // Try to pull the latest server from the cache
    //   The CacheObject.get() automatically caches the object if it's
    //   not found in the cache.
    const server = await serverCacheManager.get(message.guild, true)
    // The cache manager failed to find / create the server.
    if (!server) {
        client.logger.error(
            `Server ${message.guild.id} couldn't be initialized in the database or cache.`
        )
        return
    }
    // Use the prefix either from the server preferences or the bot's default prefix
    const prefix = server.prefix ? server.prefix : DEFAULT_CMD_PREFIX

    // Ignore messages not starting with the default prefix
    if (message.content.indexOf(prefix) !== 0) return
    // Our standard argument/command name definition.
    const args = message.content
        .slice(prefix.length)
        .trim()
        .split(/ +/g)
    const command = args.shift().toLowerCase()
    // Grab the command data from the commands collection.
    const cmd = client.commands.get(command)
    // If that command doesn't exist, silently exit and do nothing
    if (!cmd) return

    // Try to pull the user from the cache.
    // TODO: For future, check here if the user is allowed to access this command.
    const user = await userCacheManager.get(message.member, message.guild, true)
    // The cache manager failed to find / create the user
    if (!user) {
        client.logger.error(
            `User ${message.author.id} couldn't be initialized in the database or cache.`
        )
        return
    }

    // Run the command
    cmd.run(client, { server: server, user: user }, message, args)
}
