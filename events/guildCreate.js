const { initializeServer } = require('../database/initializers/initServer')

module.exports = async (client, guild) => {
    try {
        // Initialize a new discord server in our database.
        const server = await initializeServer(guild)

        // Run if we created a new server entry in the database.
        // TODO: If we didn't, then it already exists. Add logic to handle
        ///  this case. We should make sure the deletedAt column is set back to null.
        if (server) {
            client.logger.info(
                `Created entry in database for new guild with id ${server.id}`
            )
            guild.owner
                .send(
                    `Thanks for adding 📅 EventBot, we successfully set up your server up.`
                )
                .catch(sendErr => {
                    client.logger.warning(
                        `Failed sending guild initialization success message to owner ${guild.ownerID} in guild ${guild.id}: ${sendErr}`
                    )
                })
        }
    } catch (err) {
        client.logger.error(`Failed setting up guild ${guild.id}: ${err}`)
        guild.owner
            .send(
                `⚠️ We're having some issues on our backend right now. Your server wasn't initialized properly.`
            )
            .catch(sendErr => {
                client.logger.warning(
                    `Failed sending guild initialization error message to owner ${guild.ownerID} in guild ${guild.id}: ${sendErr}`
                )
            })
    }
}
