const { EVENT_EMIT_RATE } = require('../discord/client/config')
const sequelize = require('../database/connection')
const eventEmitQueue = require('../jobs/event-emit')
const { botStatusQueue, STATUS_SWITCH_OFFSET } = require('../jobs/bot-status')

module.exports = async bot => {
    try {
        await sequelize.authenticate()
        bot.logger.info('Connection to Postgres database successful.')
    } catch (error) {
        bot.logger.error('Unable to connect to Postgres database:', error)
    }

    // Starting our event emit cron job.
    eventEmitQueue.add(
        {
            client: bot,
        },
        {
            repeat: {
                every: EVENT_EMIT_RATE,
            },
            removeOnComplete: false,
            removeOnFail: false,
        }
    )

    // Starting the bot presence update queue.
    botStatusQueue.add(
        {},
        {
            repeat: {
                every: STATUS_SWITCH_OFFSET,
            },
            removeOnComplete: false,
            removeOnFail: false,
        }
    )

    bot.logger.info(`${bot.user.tag} is ready`)
}
