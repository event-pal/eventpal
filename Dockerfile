# We use node-alpine to minimize image footprints.
FROM node:12.16.1-alpine

# Setting env variables for docker
ENV HOME /usr/src

WORKDIR ${HOME}/app

# Copying over the dependency files from yarn and node. This allows us
# to leverage Docker's caching stage.
COPY ["package.json", "yarn.lock", "${HOME}/app/"]

# Installing all dependencies with yarn
RUN if [ "$NODE_ENV" = "production" ]; then yarn install --production; \
    else yarn install; fi

# Copy all of our project files.
COPY . .

CMD if [ "$NODE_ENV" = "production" ]; then yarn start:production; \
    else yarn start:dev; fi