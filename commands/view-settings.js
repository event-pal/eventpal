const {
    GLOBAL_PERMISSIONS,
    DEFAULT_CMD_PREFIX,
} = require('../discord/client/config')
const settingsEmbed = require('../discord/embeds/settings')

/**
 * Command handler for view-settings
 */
module.exports = {
    name: 'view-settings',
    description: 'Shows you all the settings for your server.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/view/view-settings',
    example: ['view-settings'],
    usage: [`\`view-settings\``],
    run: async function(client, { server, user }, message) {
        message.reply(
            settingsEmbed.craftEmbed(
                `https://cdn.discordapp.com/avatars/${client.user.id}/${client.user.avatar}.jpg`,
                `https://cdn.discordapp.com/avatars/${message.author.id}/${message.author.avatar}.jpg`,
                message.author.username,
                [
                    {
                        setting: 'Server Timezone\n(--serverTimezone)',
                        value: server.preferredTimezone,
                    },
                    {
                        setting: 'Your Timezone\n(--myTimezone)',
                        value: user.timezone ? user.timezone : 'None',
                    },
                    {
                        setting: 'Command Prefix\n(--prefix)',
                        value: server.prefix || DEFAULT_CMD_PREFIX,
                    },
                ]
            )
        )
    },
}
