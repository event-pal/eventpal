const ServerModel = require('../models/Servers')
const EventModel = require('../models/Event')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const moment = require('moment')
const { Op } = require('sequelize')

// Date format
const DATE_FORMAT = 'LLL'

// The default options we use to render the user / guild avatar.
const defaultIconOptions = {
    format: 'jpg',
    dynamic: true,
    size: 512,
}

/**
 * Command handler for view-event
 */
module.exports = {
    name: 'view-events',
    description: 'Shows all events the calling server owns.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/view/view-events',
    example: ['view-events'],
    usage: [`\`view-events\``],
    run: async function(client, { user }, message) {
        try {
            // Getting the server and events for that server.
            const server = await ServerModel.Server.findOne({
                where: {
                    id: message.guild.id,
                },
                include: [
                    {
                        model: EventModel.Event,
                        where: {
                            deletedAt: {
                                [Op.eq]: null,
                            },
                        },
                    },
                ],
                order: [[EventModel.Event, 'nextEventTrigger', 'ASC']],
            })

            // The parsed events
            let events = null

            if (server && server.events && server.events.length > 0) {
                // The events
                events = server.events.map(event => {
                    return `›› **${event.eventName}#${event.id}** - ${
                        event.type
                    }\n\`🕑 ${
                        event.nextEventTrigger
                            ? moment(event.eventDate)
                                  .tz(user.timezone || server.preferredTimezone)
                                  .format(DATE_FORMAT)
                            : 'Never'
                    }\`\n\n`
                })
            }

            message.reply(
                infoEmbed.craftEmbed({
                    guildIcon: message.guild
                        ? message.guild.iconURL(defaultIconOptions)
                        : null,
                    infoTitle: `Events ›› ${message.guild.name}`,
                    infoDescription: events
                        ? `\`📅 ${server.events.length} events found\`, \`⚠️ ${
                              server.eventsLimit
                          } event limit\`, \`🕑 ${user.timezone ||
                              server.preferredTimezone}\`\n\n${events.join('')}`
                        : `Looks like you don't have any events created.`,
                })
            )
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: 'Retrieving Events from Database',
                    description: err.message,
                })
            )
        }
    },
}
