const EventModel = require('../models/Event')
const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const moment = require('moment')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const { validateEventId } = require('../models/validators/validators')

// Date format
const DATE_FORMAT = 'LLL'

// The default options we use to render the user / guild avatar.
const defaultIconOptions = {
    format: 'jpg',
    dynamic: true,
    size: 512,
}

/**
 * Command handler for view-event
 */
module.exports = {
    name: 'view-event',
    description: 'Shows information about a specific event.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/view/view-event',
    example: ['view-event 142'],
    usage: [
        `\`view-event <ID>\``,
        `\`ID\` The id of the event. See the view-events command.`,
    ],
    run: async function(client, { user, server }, message, [id, ..._]) {
        // Errors parsing arguments
        let errors = []

        // Validating arguments
        errors = errors.concat(validateEventId(id))

        if (errors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: errors,
                })
            )
            return
        }

        try {
            const event = await EventModel.Event.findOne({
                where: {
                    serverId: message.guild.id,
                    id: id,
                },
            })

            if (event) {
                message.reply(
                    infoEmbed.craftEmbed({
                        guildIcon: message.guild
                            ? message.guild.iconURL(defaultIconOptions)
                            : null,
                        infoTitle: `Event ›› ${event.eventName}#${event.id}`,
                        infoDescription: `Channel: <#${
                            event.channelId
                        }>\nAuthor: <@${event.author}> Editor: <@${
                            event.modifiedBy
                        }>\nType: **${
                            event.type
                        }**\n\nDisplay Timezone: ${user.timezone ||
                            server.preferredTimezone}\nStart Date: \`🕑 ${moment(
                            event.eventDate
                        )
                            .tz(user.timezone || server.preferredTimezone)
                            .format(DATE_FORMAT)}\`\nNext Notification: \`🕑 ${
                            event.nextEventTrigger
                                ? moment(event.nextEventTrigger)
                                      .tz(
                                          user.timezone ||
                                              server.preferredTimezone
                                      )
                                      .format(DATE_FORMAT)
                                : 'Never'
                        }\`\n\n${event.msg}`,
                    })
                )
            } else {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: 'Invalid Event Id',
                        description:
                            "That event either doesn't exist or doesn't belong to you.",
                    })
                )
            }
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: 'Retrieving Events from Database',
                    description: err.message,
                })
            )
        }
    },
}
