const ServerModel = require('../models/Servers')
const UserModel = require('../models/User')
const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const ServerCache = require('../cache/routines/server-cache')
const UserCache = require('../cache/routines/user-cache')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const {
    validateCommandPrefix,
    validateTimezone,
} = require('../models/validators/validators')
const {
    parse,
    craftParsedNameSpaces,
} = require('../discord/client/positional-parser')

// The server cache manager
const serverCacheManager = new ServerCache()
// The user cache manager
const userCacheManager = new UserCache()

// Edit settings command layout
const settingsCommandLayout = {
    serverTimezone: {
        validator: validateTimezone,
        fieldName: 'preferredTimezone',
        nameSpace: 'server',
    },
    prefix: {
        validator: validateCommandPrefix,
        nameSpace: 'server',
    },
    myTimezone: {
        validator: validateTimezone,
        fieldName: 'timezone',
        nameSpace: 'user',
    },
}

// The parser options
const options = {
    requireOne: true,
}

/**
 * Command handler for view-settings
 */
module.exports = {
    name: 'edit-setting',
    description: 'Shows you all the settings for your server.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/edit/edit-setting',
    example: [
        '!edit-setting --myTimezone America/Los_Angeles',
        '!edit-setting --prefix $ --serverTimezone UTC',
    ],
    usage: [
        `\`edit-setting --<FLAG> <VALUE> ...\``,
        `\`--serverTimezone\` The server's preferred timezone`,
        `\`--myTimezone\` Your preferred timezone`,
        `\`--prefix\` The prefix the bot should use`,
    ],
    run: async function(client, _, message, positionals) {
        // Parse the command
        const [parsed, errors] = parse(
            settingsCommandLayout,
            options,
            positionals
        )

        // What are we updating for the user, and what are we updating
        // for the server?
        const nameSpaces = craftParsedNameSpaces(settingsCommandLayout, parsed)

        // Before we make any database calls, let's fail the command
        // if any of the arguments are incorrect.
        if (errors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: errors,
                })
            )

            return
        }

        try {
            // Get the event from the database.
            const server = await ServerModel.Server.findOne({
                where: {
                    id: message.guild.id,
                },
                include: [
                    {
                        model: UserModel.User,
                        where: {
                            id: message.author.id,
                        },
                    },
                ],
            })

            // We have some values changed for the user.
            if (nameSpaces.user) {
                const updatedUser = await server.users[0].update(
                    nameSpaces.user
                )
                userCacheManager.set(updatedUser.toJSON())
            }

            // We have some values changed for the server.
            if (nameSpaces.server) {
                const updatedServer = await server.update(nameSpaces.server)
                serverCacheManager.set(updatedServer.toJSON())
            }

            message.reply(
                infoEmbed.craftEmbed({
                    infoTitle: `Modified Settings`,
                    infoDescription: `Succesfully modified your setting.`,
                })
            )
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: 'Database Error Fetching Server',
                    description: `${err.message}`,
                })
            )
        }
    },
}
