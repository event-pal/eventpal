const EventModel = require('../models/Event')
const infoEmbed = require('../discord/embeds/info')
const moment = require('moment')
const errorEmbed = require('../discord/embeds/error')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const { Op } = require('sequelize')
const {
    Flag,
    PositionalParser,
} = require('../discord/client/positional-parser')

// The format we expect the date to be in.
const EVENT_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH:mm:ss'
const CMD_SEPARATOR = '--'
// The flags we allow for this command
const CMD_FLAGS = [
    new Flag('channelId'),
    new Flag('type', EventModel.validators.type),
    new Flag('eventName', EventModel.validators.eventName),
    new Flag('eventDate'),
    new Flag('msg', EventModel.validators.msg),
]

const parser = new PositionalParser(CMD_FLAGS, CMD_SEPARATOR)

/**
 * Command handler for view-event
 */
module.exports = {
    name: 'edit-event',
    description: 'Allows you to edit an event.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/edit/edit-event',
    example: [
        'edit-event 42 --channelId #general --eventName New Name!\n',
        'edit-event 43 --eventDate 2040-01-05 11:00:00 --msg A time in the future!',
    ],
    usage: [
        `\`edit-event <ID> --<flag> <VALUE> ...\`\n`,
        `\`ID\` The id of the event. See command view-events`,
        `\`--type\` How often this event triggers. ONCE, HOURLY, WEEKLY, MONTHLY, YEARLY`,
        `\`--channelId\` A discord channel to send the reminder to`,
        `\`--eventName\` The name of the event`,
        `\`--msg\` The event message`,
    ],
    run: async function(client, { server }, message, [id, ...positionals]) {
        // The parsed command
        let parsedCommand = parser.parse(positionals)
        // The channels mentioned by this user in this message
        const mentionedChannels = message.mentions.channels.array()
        // Push any errors parsing the id of the event.
        parsedCommand.errors = parsedCommand.errors.concat(
            EventModel.validators.id(id).errs
        )

        // Parsing date manually
        if (parsedCommand.parsed.eventDate) {
            parsedCommand.errors.concat(
                EventModel.validators.eventDate(
                    parsedCommand.parsed.eventDate,
                    EVENT_TIMESTAMP_FORMAT,
                    server.preferredTimezone
                )
            )
        }

        // If at this point our current modifier hasn't switched we can assume
        // there were no positional arguments passed in.
        if (Object.keys(parsedCommand.parsed).length === 0) {
            parsedCommand.errors.push({
                title: 'Positional Arguments',
                msg: `At least 1 valid position argument must be passed in for this command.`,
            })
        } else {
            // Validating channel id if it exists.
            // We need a custom validator for this because there may be multiple channels tagged
            // but we need the one that comes strictly after the --channel flag.
            if (parsedCommand.parsed.channelId !== undefined) {
                if (parsedCommand.parsed.channelId.length !== 0) {
                    // Get the raw channel string without the discord formatting.
                    let rawChannel = parsedCommand.parsed.channelId.substring(
                        parsedCommand.parsed.channelId.lastIndexOf('<#') + 2,
                        parsedCommand.parsed.channelId.lastIndexOf('>')
                    )
                    // Find all the channels tagged with the matching name.
                    const channelMatch = mentionedChannels.filter(
                        c => c.id.toString() === rawChannel
                    )

                    // If our channel name didn't match the positional argument
                    if (channelMatch.length === 0) {
                        parsedCommand.errors.push({
                            title: 'Invalid Channel',
                            msg:
                                "The tagged channel didn't match the value in the positional argument.",
                        })
                    } else {
                        // Setting another field with the matching channel id.
                        parsedCommand.parsed.channelId = rawChannel
                    }
                } else {
                    parsedCommand.errors.push({
                        title: 'Invalid Channel',
                        msg:
                            'After the channel positional argument we expect a single channel.',
                    })
                }
            }
        }

        // Before we make any database calls, let's fail the command
        // if any of the arguments are incorrect.
        if (parsedCommand.errors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: parsedCommand.errors,
                })
            )

            return
        }

        try {
            // Do the update.
            const event = await EventModel.Event.findOne({
                where: {
                    id: id,
                    deletedAt: {
                        [Op.eq]: null,
                    },
                    serverId: {
                        [Op.eq]: message.guild.id,
                    },
                },
            })

            // If we found that event, then let's process it.
            if (event) {
                // Setting the author id.
                parsedCommand.parsed.modifiedBy = message.author.id

                if (parsedCommand.parsed.eventDate) {
                    parsedCommand.parsed.eventDate = moment
                        .tz(
                            parsedCommand.parsed.eventDate,
                            EVENT_TIMESTAMP_FORMAT,
                            server.preferredTimezone
                        )
                        .format()

                    // Updating the according trigger.
                    parsedCommand.parsed.nextEventTrigger =
                        parsedCommand.parsed.eventDate
                }

                // Update our event.
                await event.update(parsedCommand.parsed)

                // Send the send the reply
                message.reply(
                    infoEmbed.craftEmbed({
                        infoTitle: `Modified Event: ${event.eventName}#${event.id}`,
                        infoDescription: `Your event was succesfully modified and saved.`,
                    })
                )
            } else {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: `Event Not Found`,
                        description: `An event with that id either doesn't exist, or doesn't belong to you.`,
                    })
                )
            }
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Database Error Inserting Event`,
                    description: `${err.message}`,
                })
            )
        }
    },
}
