const errorEmbed = require('../discord/embeds/error')
const { validateLogFilePath } = require('../models/validators/validators')
const { parse } = require('../discord/client/positional-parser')
const { GLOBAL_PERMISSIONS, LOGS_DIR } = require('../discord/client/config')

// Edit settings command layout
const devLogDumpCommandLayout = {
    file: {
        validator: validateLogFilePath,
        required: true,
    },
}

// The parser options
const options = {
    requireOne: true,
}

/**
 * Command handler for view-settings
 */
module.exports = {
    name: 'dev-log-dump',
    description: 'Dumps a specified log file (developer use only)',
    globalPermission: GLOBAL_PERMISSIONS.DEVELOPER,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/develop/dev-log-dump',
    example: ['dev-log-dump --file some-application.log'],
    usage: [
        `\`dev-log-dump --file <FILE_NAME>\``,
        `\`--file\` The name of the log file. View with dev-log-view`,
    ],
    run: async function(client, { user }, message, args) {
        // This is a developer only command. If the user issuing this command does not
        // have the developer level permissions, we simply ignore this request silently.
        //   For safety we also check if the user was empty somehow.
        if (
            !user ||
            !user.globalPermission ||
            !(user.globalPermission & GLOBAL_PERMISSIONS.DEVELOPER)
        )
            return

        // Parse the command
        const [parsed, errors] = parse(devLogDumpCommandLayout, options, args)

        if (errors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: errors,
                })
            )
            return
        }

        await message.reply('Sending log dump...', {
            files: [`${LOGS_DIR}${parsed.file}`],
        })
    },
}
