const EventModel = require('../models/Event')
const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const moment = require('moment')
const { Op } = require('sequelize')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const { validateDeleteType } = require('../models/validators/validators')
const { parse } = require('../discord/client/positional-parser')

// Edit settings command layout
const deleteCommandLayout = {
    type: {
        validator: validateDeleteType,
        default: '',
    },
}

// The parser options
const options = {
    requireOne: false,
}

/**
 * Command handler for delete-event
 */
module.exports = {
    name: 'delete-event',
    description: 'Allows you to delete an event.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/delete/delete-event',
    example: ['!delete-event 42', '!delete-event 43 --type hard'],
    usage: [
        `\`delete-event <ID> --type <soft | hard>\``,
        '`ID` The id of the event. Find the id with the view-events command',
        '`--type` (optional) The type of delete to make',
    ],
    run: async function(client, _, message, [id, ...positionals]) {
        // Parse the command
        const [parsed, errors] = parse(
            deleteCommandLayout,
            options,
            positionals
        )

        // The parsed command
        //let parsedCommand = parser.parse(positionals)

        if (errors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: errors,
                })
            )

            return
        }

        try {
            // Get the event from the database.
            const event = await EventModel.Event.findOne({
                where: {
                    id: id,
                    serverId: {
                        [Op.eq]: message.guild.id,
                    },
                },
            })

            if (event) {
                // We delete the event differently based on the type.
                if ((parsed.type || 'soft') === 'soft') {
                    event.update({
                        deletedAt: moment(),
                    })
                } else {
                    event.destroy()
                }

                // Send the send the reply
                message.reply(
                    infoEmbed.craftEmbed({
                        infoTitle: `Deleted Event #${id}`,
                        infoDescription: `You performed a ${parsed.type ||
                            'soft'} delete. A soft delete means you can recover this event later. A hard delete means it's permanently gone.`,
                    })
                )
            } else {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: `Event Not Found`,
                        description: `An event with that id either doesn't exist, or doesn't belong to you.`,
                    })
                )
            }
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Database Error Deleting Event`,
                    description: `${err.message}`,
                })
            )
        }
    },
}
