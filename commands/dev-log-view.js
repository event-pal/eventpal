const fs = require('fs')
const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')

// Finds an instance of an error (error / fatal) in the log file
const errorRegex = /\[(ERROR|FATAL)\] default/g

/**
 * Command handler for view-settings
 */
module.exports = {
    name: 'dev-log-view',
    description:
        'Views all the log files available to be dumped. (developer use only)',
    globalPermission: GLOBAL_PERMISSIONS.DEVELOPER,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/develop/dev-log-view',
    example: ['dev-log-view'],
    usage: [`\`dev-log-view\``],
    run: async function(client, { user }, message) {
        // This is a developer only command. If the user issuing this command does not
        // have the developer level permissions, we simply ignore this request silently.
        //   For safety we also check if the user was empty somehow.
        if (
            !user ||
            !user.globalPermission ||
            !(user.globalPermission & GLOBAL_PERMISSIONS.DEVELOPER)
        )
            return

        // We want to log this action. It's a high security thing.
        client.logger.info(
            `user#${user.id} with permission level ${user.globalPermission} requested to view developer logs, and was granted access.`
        )

        fs.readdir('./logs/', async (err, files) => {
            if (err) {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: 'Error Reading Logs',
                        description:
                            'This is probably due to an issue to file permissions on the remote server.',
                        errors: [
                            {
                                title: 'Failed Reading Log Directory',
                                msg: err.message,
                            },
                        ],
                    })
                )
                return
            }

            // The formatted log name string.
            let logNames = ''

            files.forEach(file => {
                // Just in-case there's any exploits or someone accidentally adds bot
                // logic into this folder.
                if (!file.endsWith('.log')) return
                const stats = fs.statSync(`./logs/${file}`)
                const logErrors = (
                    fs
                        .readFileSync(`./logs/${file}`)
                        .toString()
                        .match(errorRegex) || []
                ).length

                logNames = logNames.concat(
                    `Name: \`${file}\`\n Side On Disk: \`${(
                        stats.size / 1024
                    ).toFixed(2)} KB\`\nErrors: ${
                        logErrors > 0 ? `\`⚠️ ${logErrors}\`` : `\`✔️ 0\``
                    }\n\n`
                )
            })

            await message.reply(
                infoEmbed.craftEmbed({
                    infoTitle: 'Developer Log View',
                    infoDescription:
                        logNames.length > 0 ? logNames : 'No log files found.',
                })
            )
        })
    },
}
