const EventModel = require('../models/Event')
const moment = require('moment')
const errorEmbed = require('../discord/embeds/error')
const { Op } = require('sequelize')
const { GLOBAL_PERMISSIONS } = require('../discord/client/config')
const {
    validateEventName,
    validateEventType,
    validateEventMsg,
    validateChannelId,
    validateDateFuture,
    validateTimezoneType,
} = require('../models/validators/validators')

// The format we expect the date to be in.
const EVENT_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH:mm:ss'

/**
 * Command handler for add-event
 *
 * description: Adds an event reminder for the server.
 * usage: !addevent
 *   type - ONCE | HOURLY | DAILY | WEEKLY | MONTHLY | YEARLY
 *   channelName - The name of the channel to send this message in
 *   timestamp:
 *   message - The quoted message to send as a part of this reminder.
 */
module.exports = {
    name: 'add-event',
    description: 'Adds an event reminder for the server.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL:
        'https://nafana.gitbook.io/eventpal/commands-reference/add/add-event',
    example: [
        'add-event MyTitle ONCE #general 2040-01-05 11:00:00 me Far into the future',
    ],
    usage: [
        '`add-event <TITLE> <TYPE> <CHANNEL> <TIMESTAMP> <TIMEZONE_TYPE> <MESSAGE>`',
        '`TITLE` The title of the event',
        '`TYPE` The event type: ONCE, HOURLY, WEEKLY, MONTHLY, YEARLY',
        '`CHANNEL` The channel you want this reminder to be sent to',
        '`TIMESTAMP` The time you want the event to start. In the form YYYY-MM-DD HH:mm:ss',
        "`TIMEZONE_TYPE` Which timezone preference to use: 'me', 'server'",
        '`MESSAGE` The message for your event reminder (1000 character cap)',
    ],
    run: async function(
        client,
        { server, user }, // Our cache registry
        message,
        [title, type, channel, date, time, timezoneType, ...msg]
    ) {
        // The errors we encountered while validating command arguments.
        let commandErrors = []
        // The channels mentioned by this user in this message
        const mentionedChannels = message.mentions.channels.array()
        // The timezone we're using for this event
        const tz =
            timezoneType === 'me' && user.timezone
                ? user.timezone
                : server.preferredTimezone

        commandErrors = commandErrors.concat(
            validateEventName(title),
            validateEventType(type),
            validateEventMsg(msg.join(' ')),
            validateChannelId(mentionedChannels),
            validateTimezoneType(timezoneType),
            validateDateFuture(`${date} ${time}`, EVENT_TIMESTAMP_FORMAT, tz)
        )

        // Before we make any database calls, let's fail the command
        // if any of the arguments are incorrect.
        if (commandErrors.length > 0) {
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Executing ${this.name}`,
                    description: this.usage,
                    errors: commandErrors,
                })
            )

            return
        }

        // Grabbing all of our events for this server which aren't deleted.
        try {
            const events = await EventModel.Event.findAll({
                where: {
                    serverId: server.id,
                    deletedAt: {
                        [Op.eq]: null,
                    },
                },
            })

            // Did this server hit the event limit?
            if (events && events.length >= server.eventsLimit) {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: `Event Limit Exceeded`,
                        description: `You have reached your limit of ${server.eventsLimit} events. Your currently have ${events.length} events.`,
                    })
                )
                return
            }

            // Parse the date the user gave to us, with the timezone they prefer.
            const eventDate = moment.tz(
                `${date} ${time}`,
                EVENT_TIMESTAMP_FORMAT,
                tz
            )

            /**
             * Creating the event in our database.
             *
             * TODO: It's likely that a user will want to edit their event after the realized
             *   they made a mistake. So we should also cache the event here.
             */
            EventModel.Event.create({
                serverId: server.id,
                author: user.id,
                modifiedBy: user.id,
                type: type,
                channelId: mentionedChannels[0].id,
                msg: msg.join(' '),
                eventName: title,
                eventDate: eventDate.format(),
                nextEventTrigger: eventDate.format(),
                deletedAt: null,
            }).then(event => {
                message.reply(
                    `Created a \`${event.type}\` event reminder for guild \`${
                        event.serverId
                    }\` in channel \`${
                        event.channelId
                    }\`:${channel}. It will execute on \`${eventDate.format(
                        'LLLL'
                    )} ${tz}\``
                )
            })
        } catch (err) {
            client.logger.error(
                `Command failure in ${this.name}. user#${message.author.id} supplied arguments ${message.content}: ${err}`
            )
            message.reply(
                errorEmbed.craftEmbed({
                    title: `Database Error Inserting Event`,
                    description: `${err.message}`,
                })
            )
        }
    },
}
