const errorEmbed = require('../discord/embeds/error')
const infoEmbed = require('../discord/embeds/info')
const {
    DEFAULT_CMD_PREFIX,
    PROJECT_LINK,
    SUPPORT_COMMANDS_LINK,
    SUPPORT_DISCORD_SERVER_LINK,
    GLOBAL_PERMISSIONS,
} = require('../discord/client/config')

/**
 * Command handler for view-settings
 */
module.exports = {
    name: 'help',
    description: 'Find information on all executable commands.',
    globalPermission: GLOBAL_PERMISSIONS.USER_DEFAULT,
    webdocURL: 'https://nafana.gitbook.io/eventpal/commands-reference/help',
    example: ['help', 'help view-settings'],
    usage: [
        `\`help <COMMAND_NAME>\``,
        `\`COMMAND_NAME\` A name of any valid command`,
    ],
    run: async function(client, { server, user }, message, [cmd, ..._]) {
        // If the command argument was supplied generate the help message
        // for that specific command.
        if (cmd) {
            if (!client.commands.get(cmd)) {
                message.reply(
                    errorEmbed.craftEmbed({
                        title: `Executing ${this.name}`,
                        description: `Sorry I couldn't find that command. See all available commands with \`${server.prefix ||
                            DEFAULT_CMD_PREFIX}help\``,
                    })
                )
                return
            }

            const command = require(`./${cmd}`)

            message.reply(
                infoEmbed.craftEmbed({
                    infoTitle: `Help Reference ›› ${server.prefix ||
                        DEFAULT_CMD_PREFIX}${command.name}`,
                    infoDescription: command.description,
                    fields: [
                        {
                            title: 'Usage',
                            value: `${command.usage.join('\n')}`,
                        },
                        {
                            title: 'Examples',
                            value: command.example.join('\n'),
                            inline: true,
                        },
                        {
                            title: `Permissions`,
                            value:
                                user.globalPermission & command.globalPermission
                                    ? 'Yes'
                                    : 'No',
                            inline: true,
                        },
                        {
                            title: `Web Docs`,
                            value: `Read the docs online for ${server.prefix ||
                                DEFAULT_CMD_PREFIX}${command.name} [here](${
                                command.webdocURL
                            }).`,
                        },
                    ],
                })
            )
        } else {
            // All of the command names to be displayed.
            let commandReference = ''

            client.commands.forEach(cmd => {
                const command = require(`./${cmd.name}`)
                // Ignore all dev commands by default.
                if (!command.name.startsWith('dev')) {
                    commandReference = commandReference.concat(
                        `\`${server.prefix || DEFAULT_CMD_PREFIX}${
                            command.name
                        }\` ${command.description}\n`
                    )
                }
            })

            message.reply(
                infoEmbed.craftEmbed({
                    infoTitle: 'Help Reference',
                    infoDescription: commandReference,
                    fields: [
                        {
                            title: 'Command Specific Info',
                            value: `Run \`${server.prefix ||
                                DEFAULT_CMD_PREFIX}help <COMMAND_NAME>\` to get command specific help.`,
                        },
                        {
                            title: 'Need some more help? We got you!',
                            value: `:books: [Web Docs](${SUPPORT_COMMANDS_LINK}) ›› :package: [Our Repository](${PROJECT_LINK}) ›› :speech_left: [Support Server](${SUPPORT_DISCORD_SERVER_LINK})`,
                        },
                    ],
                })
            )
        }
    },
}
