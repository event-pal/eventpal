# Change Log

## v0.6.0 - Sun Mar 29 10:54:29 PDT 2020

**Added**
- Added !help command
- Added globalPermission, webdocURL, and example properties to each command for documentation. 

## v0.5.0 - Sat Mar 28 14:45:25 PDT 2020

**Database Migrations**
- Added globalPermission column to user table

**Added**
- Added !dev-log-dump command (dev use only)
- Added !dev-log-view command (dev use only)
- Added bot-status CRON job to update the bot's presence on discord

**Changed**
- Overhauled logging functionality across the bot (added more useful & distinct logs)
- log4js logs are now written to file system and to stdout

## v0.4.2 - Thu Mar 26 20:58:56 PDT 2020

**Added**
- !view-event command

**Changed**
- !view-events embed visuals, and what content is displayed

**Fixed**
- Event reminders not sending when message contained too many fields or was over 1000 characters
- Add-Event logic passed array instead of message string to msg validator causing the incorrect length validation on messages

## v0.4.1 - Thu Mar 26 20:58:56 PDT 2020

**Added**
- Added logic to allow event reminders to ping roles properly.

**Changed**
- Formatting for event reminder now includes guild avatar

**Fixed**
- Users being able to created unlimited events. Bug was introduced in 0.3.0

## v0.3.1 - Thu Mar 26 20:58:56 PDT 2020

**Fixed**
- !view-events throwing error when user timezone not set

## v0.3.0 - Thu Mar 26 19:35:57 PDT 2020

**Database Migrations**
- Added users table
- Added userServers join table

**Fixed**
- Event date not changing when running !edit-event

**Added**
- Added user-cache manager
- Added User model
- Added UserServers model
- Added postional command parser
- Added automatic build tagging on build pipeline

**Changed**
- Initializer logic moved to /database/initializers
- Prettified embeded bot responses, changed main color to blue
- Validators moved into separate class

**Removed**
- Caching logic from command handlers moved to onMessage event

## v0.2.0 - Wed Mar 25 00:02:01 PDT 2020

**Removed**
- Dependency on module-alias


## v0.1.0 - Tue Mar 24 11:12:35 PDT 2020

**Database Migrations**
- Prefix column added to Servers table.

**Added**
- Synced Server model with database migration.
- Added logic for handling custom prefixes.
